package com.mezizy.ifanmerchant;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mezizy.ifanmerchant.global.Global;

import org.apache.http.util.EncodingUtils;


public class StoreManagementTab extends Fragment {
    public static WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_store_management, container, false);

        webView = (WebView) view.findViewById(R.id.webView);
         WebSettings webSettings = webView.getSettings();
        webView.getSettings().setBuiltInZoomControls(true);
               webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });

//        webView.loadUrl("http://120.96.99.54/WebSite/DefaultLogin");

        //post方式传送参数
        String postData = "merchantId=m001&merchantPwdSha=123";
        webView.postUrl("http://120.96.99.54/WebSite/DefaultLogin", EncodingUtils.getBytes(postData, "base64"));

        return view;

    }


}