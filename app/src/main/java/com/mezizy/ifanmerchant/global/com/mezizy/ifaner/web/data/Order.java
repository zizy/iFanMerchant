package com.mezizy.ifanmerchant.global.com.mezizy.ifaner.web.data;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Zizy on 15/2/23.
 */
public class Order {
    String tradeId;
    String cusName;
    String cusId;
    String merchantId;
    List<Dish> dishList;
    int tradeState;
    String handlerId;
    float amount;
    String createTime;
    String finishTime;
    String handleTime;
    String deliverPlace;
    String remarks;
    String merchantName;

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Order(List<Dish> dishList, String tradeId, String cusId, String cusName,String merchantId, int tradeState, float amount, String createTime, String remarks, String deliverPlace) {
        this.dishList = dishList;
        this.tradeId = tradeId;
        this.cusId = cusId;
        this.merchantId = merchantId;
        this.tradeState = tradeState;
        this.amount = amount;
        this.createTime = createTime;
        this.remarks = remarks;
        this.deliverPlace = deliverPlace;
        this.cusName=cusName;
        
    }



    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public List<Dish> getDishList() {
        return dishList;
    }

    public void setDishList(List<Dish> dishList) {
        this.dishList = dishList;
    }

    public int getTradeState() {
        return tradeState;
    }

    public void setTradeState(int tradeState) {
        this.tradeState = tradeState;
    }

    public String getHandlerId() {
        return handlerId;
    }

    public void setHandlerId(String handlerId) {
        this.handlerId = handlerId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(String handleTime) {
        this.handleTime = handleTime;
    }

    public String getDeliverPlace() {
        return deliverPlace;
    }

    public void setDeliverPlace(String deliverPlace) {
        this.deliverPlace = deliverPlace;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
}
