package com.mezizy.ifanmerchant.global.com.mezizy.ifaner.web.data;

/**
 * Created by Zizy on 15/2/11.
 */
public class Dish {

    String dishesId;
    String merchantId;
    String dishesState;
    String dishesName;
    float dishesPrice;
    String dishesDiscount;
    String iconUrl;
    float total ;

    private int cartQuantity;

    public int getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(int cartQuantity) {
        this.cartQuantity = cartQuantity;
    }
    public void setCartQuantity() {
        this.cartQuantity = cartQuantity+1;
    }
    public Dish(String dishesId, String merchantId) {
        this.dishesId = dishesId;
        this.merchantId = merchantId;
    }

    public Dish(String dishesId, String merchantId, float total, int cartQuantity) {
        this.dishesId = dishesId;
        this.merchantId = merchantId;
        this.total = total;
        this.cartQuantity = cartQuantity;
    }

    public Dish(String dishesId, String merchantId, float dishesPrice) {
        this.dishesId = dishesId;
        this.merchantId = merchantId;
        this.dishesPrice = dishesPrice;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getDishesId() {
        return dishesId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getDishesState() {
        return dishesState;
    }

    public String getDishesName() {
        return dishesName;
    }

    public float getDishesPrice() {
        return dishesPrice;
    }

    public String getDishesDiscount() {
        return dishesDiscount;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
