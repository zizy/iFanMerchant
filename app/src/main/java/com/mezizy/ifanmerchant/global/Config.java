package com.mezizy.ifanmerchant.global;

/**
 * Created by Zizy on 15/2/3.
 */
public class Config {
//    public static final String SERVER = "http://120.96.99.54/WebService/iFanerWebService.asmx/";
    public static final String SERVER = "http://120.96.99.70/iFanerWebService.asmx/";
    public static final String MOODLE_LOG_IN_URI = "http://120.96.82.71/appwebservice/wsstu.asmx/GetLoginWSs";

    public static final String IFANER_LOG_IN_URI = SERVER + "iFanerLoginCheck";
    public static final String IFAN_MERCHANT_LOG_IN_URI = SERVER + "iFanerLoginCheckMerchant";
    public static final String IFANER_GET_ORDER_LIST = SERVER + "getOrderListMerchant";
    public static final String IFANER_SET_ORDER_STATE = SERVER + "setOrderState";
    public static final String IFANER_GET_DISH_LIST = SERVER + "getListByDishes";


    public static final String IFANER_SIGN_UP_URI = "";

    public static final boolean MOODLE_LOG_IN_SUCCESS = true;
    public static final boolean MOODLE_LOG_IN_FAILED = false;

    public static final int IFANER_LOG_IN_SUCCESS = 1;
    public static final int IFANER_LOG_IN_FAILED_AU = 2;
    public static final int IFANER_LOG_IN_FAILED_INTERNET = 3;


    public static final int STATE_NOT_CONFIRM = 0;
    public static final int STATE_CONFIRMED = 1;
    public static final int STATE_FINISHED_NOT_PAY = 2;
    public static final int STATE_FINISHED_PAID = 3;
    public static final int STATE_CANCEL = 4;
    public static final int STATE_EXCEPTION = -1;

    public static final int ORDER_NUM_MAX = 6;

    public static final boolean BACKGROUND = true;
    public static final boolean FOREGROUND = false;

    public static final String ACTION_NEW_ORDER = "ActionNewOrder";

    public static final int GET_NEW_ORDER_SERVER_REPEAT_TIME=5000;

    public static final String SHARED_PREFERENCE="SharedPreference";
    public static final String SHARED_ID ="SharedMerchantId";
    public static final String SHARED_PWD_SHA ="SharedMerchantPwdSha";



}
