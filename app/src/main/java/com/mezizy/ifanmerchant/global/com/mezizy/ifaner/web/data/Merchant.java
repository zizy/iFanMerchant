package com.mezizy.ifanmerchant.global.com.mezizy.ifaner.web.data;

/**
 * Created by Zizy on 15/2/7.
 */
public class Merchant {
    private String id = "";
    private String name = "";
    private String state = "";
    private String deliveryEnable = "";
    private String discountInf = "";
    private String stars = "";
    private String iconUrl = "";

    public  String toString(){
        return String.format("id=%s, name=%s, state=%s, deliveryEnable=%s, discountInf= %s, stars=%s, inconUrl= %s", id,name,state,deliveryEnable,discountInf,stars,iconUrl);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public String getDeliveryEnable() {
        return deliveryEnable;
    }

    public String getDiscountInf() {
        return discountInf;
    }

    public String getStars() {
        return stars;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
