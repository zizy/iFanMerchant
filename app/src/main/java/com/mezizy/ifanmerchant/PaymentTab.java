package com.mezizy.ifanmerchant;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


public class PaymentTab extends Fragment {
    public    TextView edtNum;

    private static PaymentTab instance=null;

    @Override
    public void onStart() {
        super.onStart();
        instance=this;
    }

    public  static  PaymentTab getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.tab_payment, container, false);

        edtNum= ((TextView) view.findViewById(R.id.edtNum));

        return view;

    }

}