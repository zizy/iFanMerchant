package com.mezizy.ifanmerchant;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.mezizy.ifanmerchant.global.Config;
import com.mezizy.ifanmerchant.global.Global;


public class LogInActivity extends ActionBarActivity {
    EditText editStuId;
    EditText editStuPwd;

    private String merchantId;
    private String merchantPwdSha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        SharedPreferences settings = getSharedPreferences(Config.SHARED_PREFERENCE, 0); // 获取一个SharedPreferences对象

        editStuId = (EditText) findViewById(R.id.editLogInStuId);
        editStuPwd = (EditText) findViewById(R.id.editLogInStuPwd);


        String userId = settings.getString(Config.SHARED_ID, ""); // 取出保存的NAME
        String userPwd = settings.getString(Config.SHARED_PWD_SHA, ""); // 取出保存的PASSWORD
        // Set value
        editStuId.setText(userId); // 将取出来的用户名赋予field_name
        editStuPwd.setText(userPwd); // 将取出来的密码赋予filed_pass

        getSupportActionBar().hide();

        (findViewById(R.id.btnLogIn)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        ((RelativeLayout) v).setBackgroundResource(R.drawable.log_in_button_dark);

                        break;

                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:

                        ((RelativeLayout) v).setBackgroundResource(R.drawable.log_in_button);
                        break;
                }
                return false;
            }
        });
    }

    public void btnSignUpClicked(View view) {
//        startActivity(new Intent(LogInActivity.this, SignUpActivity.class));

    }

    @Override
    protected void onStart() {
        super.onStart();
        Global.appContext = getApplicationContext();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    public void logInBtnClicked(View view) {
        merchantId = editStuId.getText().toString();
        merchantPwdSha = Global.encrypt(editStuPwd.getText().toString());

        /*
        test
         */
        if (merchantId.equals("")) {
//            Global.showToast("got here!");
            Global.merchantId = "m001";
            Global.merchantPwdSha = "123";

            startActivity(new Intent(LogInActivity.this, MainActivity.class));

            return;
        }


        new Thread(new Runnable() {
            @Override
            public void run() {
                int result = Global.merchantAuthorize(merchantId, merchantPwdSha);
                if (result == Config.IFANER_LOG_IN_SUCCESS) {
//                    Global.showToast("Welcome!", LogInActivity.this);

                    Global.merchantId = LogInActivity.this.merchantId;
                    Global.merchantPwdSha = LogInActivity.this.merchantPwdSha;


                    // 首先获取一个SharedPreferences对象
                    SharedPreferences settings = getSharedPreferences(Config.SHARED_PREFERENCE, 0);
                    // 将用户名和密码保存进去
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString( Config.SHARED_ID, Global.merchantId);
                    editor.putString( Config.SHARED_PWD_SHA, editStuPwd.getText().toString());
                    editor.commit();


                    startActivity(new Intent(LogInActivity.this, MainActivity.class));

                } else if (result == Config.IFANER_LOG_IN_FAILED_AU) {
                    Global.showToast("Log in failed! Please check your id and password. ", LogInActivity.this);
                } else if (result == Config.IFANER_LOG_IN_FAILED_INTERNET) {
                    Global.showToast("Log in failed! Please check the internet connection. ", LogInActivity.this);
                }

            }
        }).start();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
